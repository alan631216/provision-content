---
Name: "redhat/subscription-gpg-keys"
Description: "An array of Yum GPG Repo Keys to import"
Documentation: |
  This is an array of strings where each string is a Yum Repo GPG key
  to import.  Specify either rendered keys (file path) on the system, or
  a URL reference to the key.

  If not specified, the primary Redhat keys will be added, as referenced
  at <https://access.redhat.com/security/team/key>

  Note that the default keys may all fail to import on RHEL Server 9, the
  failure is not fatal to the running task.  RHEL Server 9 and newer
  enforces that SHA1 signed keys and encryption algorithms can not be
  used.  Redhat's own Security keys are all SHA1 signed keys.

  It is possible to forcibly enable installation of GPG keys that are
  signed with a SHA1 hash function.  This is done setting the
  policy as follows:

    * `update-crypto-policies --set DEFAULT:SHA1`

  To return the system to the default policy, not allowing use of the
  SHA1 hash function, do:

    * `update-crypto-policies --set DEFAULT`

  The Param `redhat/subscription-crypto-policy-override` can be set with the
  policy changes to override the key import.  To do so, set the
  Param to `DEFAULT:SHA1` (or any other supported `--set` directive.
  NOTE that the `--set DEFAULT` directive will immediately be
  reset at the end of the task run.

Meta:
  icon: "lock"
  color: "blue"
  title: "Digital Rebar Community Content"
Schema:
  type: "array"
  items:
    type: "string"
  default:
    - https://access.redhat.com/security/data/fd431d51.txt
    - https://access.redhat.com/security/data/55A34A82.txt
    - https://access.redhat.com/security/data/5a6340b3.txt
    - https://access.redhat.com/security/data/d4082792.txt
    - https://access.redhat.com/security/data/37017186.txt
    - https://access.redhat.com/security/data/2fa658e0.txt
    - https://access.redhat.com/security/data/8366b0d9.txt
    - https://access.redhat.com/security/data/f21541eb.txt
#  bad keys - produces "not armored"
#    - https://access.redhat.com/security/data/8a828aad.txt
#    - https://access.redhat.com/security/data/1c711042.txt
