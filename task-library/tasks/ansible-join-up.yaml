---
Name: "ansible-join-up"
Description: "Join DRP via Ansible"
Documentation: |
  Runs an embedded Ansible Playbook to run the DRP join-up process.

  Requires an `ansible` context.

  Expects to be in a Workflow that allows the joined machine to continue
  Discovery and configuration steps as needed.

  Expects to have `rsa-key-create` run before stage is called and
  the public key MUST be on the target machine.

  Idempotent - checks to see if service is installed and will not re-run join-up.

RequiredParams:
  - rsa/key-private
  - rsa/key-name
  - rsa/key-user
Templates:
  - Contents: |-
      #!/bin/bash
      # RackN Copyright 2020

      set -e

      {{template "setup.tmpl" .}}

      {{ if empty .Machine.Address }}
      echo "ERROR: missing Machine Address - cannot proceed"
      exit 1
      {{ end }}

      # we need a keypair for Ansible
      echo "Retrieving SSH key from Machine Params rsa/key-*"
      tee {{.Param "rsa/key-name"}} >/dev/null << EOF
      {{if contains "|" (.Param "rsa/key-private") -}}
      {{.Param "rsa/key-private" | replace "|" "\n" }}
      {{else -}}
      {{.Param "rsa/key-private" | replace "KEY-----" "KEY-----\n" | replace "-----END" "\n-----END" }}
      {{- end}}
      EOF
      chmod 600 {{.Param "rsa/key-name"}}
      # some keys require \n to separate headers
      {{ $user := .Param "rsa/key-user" }}

      export ANSIBLE_HOST_KEY_CHECKING=False

      ## Build Playbook
      echo "Building from Join-Up Playbook"
      rm join-up.yaml || true
      tee join-up.yaml >/dev/null << EOF
      ---
      ### Run Join-Up via Ansible
      - hosts: all
        remote_user: "{{$user}}"
        connection: "ssh"
        gather_facts: false
        debugger: {{ if .Param "rs-debug-enable" }}on_failed{{ else }}never{{ end }}
        become: true

        tasks:

          - name: Wait for the instances to boot by checking the ssh port
            wait_for_connection:
              timeout: 300

          {{if .ParamExists "access-keys"}}
          {{range $name, $key := .Param "access-keys"}}
          - name: Set authorized key from access-keys
            authorized_key:
              user: "{{$user}}"
              state: present
              key: "{{$key}}"
          {{end}}
          {{end}}

          {{if .ParamExists "access-keys-global"}}
          {{range $name, $key := .Param "access-keys-global"}}
          - name: Set authorized key from access-keys-global
            authorized_key:
              user: "{{$user}}"
              state: present
              key: "{{$key}}"
          {{end}}
          {{end}}

          {{if .ParamExists "access-keys-shared"}}
          {{range $name, $key := .Param "access-keys-shared"}}
          - name: Set authorized key from access-keys-shared
            authorized_key:
              user: "{{$user}}"
              state: present
              key: "{{$key}}"
          {{end}}
          {{end}}

          - name: check if drpcli is running (makes join idempotent)
            service_facts:

          - name: "drpcli is installed? - skip if drpcli is installed"
            debug:
              msg: "DRPCLI status is {{`{{ services['drpcli.service'].status }}`}}"
            when: "'drpcli.service' in services"

          - name: "create uuid file for {{.Machine.Uuid}}"
            become: true
            copy:
              content: "{{.Machine.Uuid}}"
              dest: /etc/rs-uuid
            when: "'drpcli.service' not in services"

          - name: "download join-up script from {{.ProvisionerHostURL}}"
            become: true
            get_url:
              url: "{{.ProvisionerHostURL}}/machines/join-up.sh"
              dest: "./join-up.sh"
              mode: 0755
              validate_certs: false
            when: "'drpcli.service' not in services"

          - name: run join-up script
            become: true
            shell: "sudo ./join-up.sh > join-up.log 2> join-up.error"
            when: "'drpcli.service' not in services"

          - name: wait for joinup
            pause:
              seconds: 10
            when: "'drpcli.service' not in services"

          - name: check for drpcli pids (makes sure we started)
            shell: pidof drpcli
            register: drpcli_pids
            ignore_errors: true

          - name: Printing the process IDs obtained
            debug:
              msg: "PIDS of DRPCLI: {{`{{drpcli_pids.stdout}}`}}"

          - name: rerun join-up script if no pids - skip is normal
            become: true
            shell: "sudo ./join-up.sh > join-up2.log 2> join-up2.error"
            when: drpcli_pids.stdout == ""

          - name: wait for joinup second time
            pause:
              seconds: 10
            when: drpcli_pids.stdout == ""

          - name: last chance for pids of drpcli [fail if none]
            shell: pidof drpcli
            when: "'drpcli.service' not in services"
      EOF

      {{ if .Param "rs-debug-enable" }}
      ANSIBLE_DEBUG=1
      echo "========= DEBUG output playbook ========="
      cat join-up.yaml
      echo "========= DEBUG output playbook ========="
      {{ end }}

      echo "Prevent the task list from restarting!"
      drpcli machines set $RS_UUID param start-over to false > /dev/null

      echo "REMINDER: SSH user, {{ .Param "rsa/key-user" }}, must be available on target o/s!"

      echo "Run Join-Up Playbook using $(ansible-playbook --version)"
      ansible-playbook \
        -i {{ .Machine.Address }}, \
        --private-key={{.Param "rsa/key-name"}} \
        join-up.yaml

      echo "Done"
    Name: "Run Playbooks"
Meta:
  icon: "paper plane"
  color: "blue"
  title: "Digital Rebar Community Content"
  copyright: "RackN 2020"
  feature-flags: "sane-exit-codes"
