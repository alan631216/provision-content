---
Name: cloud-inventory
Description: "Inventory cloud metadata"
Documentation: |
  Collect internal API information about a cloud instance

  Requires `cloud/provider` to be set correctly.  Cloud provisioners should set this field
  automatically.  You can use `cloud-detect-meta-api` to discover the `cloud/provider` by inspection.

  If `cloud/provider` is not set then does nothing.

  Depending on the cloud provider, sets discovered `cloud/*` data from the cloud's discovery API including:

    * `public-ipv4`
    * `public-hostname`
    * `instance-type`
    * `placement/availability-zone`

  !!! note
      Will throw an error if the reported instance-id does not known `cloud/instance-id`
RequiredParams:
  - cloud/meta-apis
OptionalParams:
  - cloud/provider
Templates:
  - Name: "cloud-inspect"
    Contents: |
      #!/bin/bash
      # RackN Copyright 2020

      set -e
      {{template "setup.tmpl" .}}

      {{ if not (empty .Machine.Context) }}
      echo "No Action: this task only runs in Machine Context."
      exit 0
      {{ end }}

      {{ if not (.ParamExists "cloud/provider") }}
      echo "Skipping: cloud/provider not set (use cloud-detect-meta-api to inspect)"
      {{ else }}

      {{   $cloud := (.Param "cloud/provider") }}

      # Ubuntu Path is different than Centos Path - fix it.
      export PATH=$PATH:/usr/bin:/usr/sbin:/bin:/sbin

      {{   $apis := (get (.Param "cloud/meta-apis") "apis") }}

      echo "=== {{ upper $cloud }} INSPECT ==="
      {{   if hasKey $apis $cloud }}
      {{     $curls := get $apis $cloud }}
      {{     $id := (get (.Param "cloud/meta-apis") "id") }}
      echo "  testing meta-api for {{$id}} with {{ get $curls $id }}"
      if {{ get $curls $id }} > /dev/null ; then
        echo "  confirmed: meta-api available for {{$cloud}}"
      {{     range $k, $v := $curls }}
          echo "  retrieving: {{$k}} via {{$v | replace "\"" "'"}}"
          value=$({{$v}})
      {{       if eq $k $id }}
      {{         if $.ParamExists $id }}
          if [[ "$value" == "{{$.Param $id}}" ]]; then
            echo "  verified: Instance ID matches"
          else
            job_error "ERROR: expected {{$id}} of {{$.Param $id}} does not match reported ID of $value"
          fi
      {{         end }}
      {{       end }}
          echo "    setting: {{$k}} to $value"
          drpcli machines set $RS_UUID param {{$k}} to "\"$value\"" > /dev/null
          unset value
      {{     else }}
        echo "WARNING: No meta api calls defined for {{$cloud}}"
      {{     end }}

      else
        echo "ERROR: {{$cloud}} meta api did not respond to {{ get $apis $id }}"
        exit 1
      fi
      {{   else }}
      echo "Skipping: no defined cloud inventory for {{$cloud}} - please add to cloud/meta-apis"
      {{   end }}
      {{ end }}

      echo "done"
      exit 0
Meta:
  type: "fetch"
  icon: "cloud"
  color: "blue"
  title: "RackN Content"
  feature-flags: "sane-exit-codes"
