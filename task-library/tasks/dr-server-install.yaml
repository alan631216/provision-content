---
Name: "dr-server-install"
Description: "Install Digital Rebar Server"
Documentation: |
  Installs DRP Server.  Sets DRP-ID to Machine.Name

  LIMITATIONS:

    * firewall features only available for Centos family

  The primary use cases for this task are

    1. drp-server pipline

  Will transfer the DRP license to the machine being created.

  For operators, this feature makes it easy to create new edge sites using DRP Manager.

ExtraClaims:
  - scope: "files"
    action: "*"
    specific: "*"
  - scope: "contents"
    action: "*"
    specific: "rackn-license"
  - scope: "endpoints"
    action: "*"
    specific: "*"
OptionalParams:
  - "dr-server/install-drpid"
  - "dr-server/initial-password"
  - "dr-server/initial-user"
  - "dr-server/initial-version"
  - "dr-server/skip-port-check"
  - "dr-server/install-airgap"
  - "dr-server/airgap-file-url"
Templates:
  - Name: "drp-install"
    Contents: |-
      #!/usr/bin/env bash

      set -e
      {{ template "setup.tmpl" .}}
      {{ template "download-tools.tmpl" . }}

      # Firewall ports update
      if which firewall-cmd 2>/dev/null >/dev/null ; then
        if [[ "$(firewall-cmd --state 2>&1)" != "not running" ]] ; then
          job_info "Update firewall ports"
          {{range $index, $port := (.ComposeParam "network/firewall-ports" | uniq)}}
          firewall-cmd --permanent --add-port={{$port}}
          {{ end }}
          firewall-cmd --reload
        else
          job_info "Firewall is not running - don't update ports"
        fi
      else
          job_info "Firewall is installed - don't update ports"
      fi

      # Set up username and password
      DRP_PASSWORD=""
      {{ if .ParamExists "dr-server/initial-password" }}
      DRP_PASSWORD="--drp-password={{.Param "dr-server/initial-password"}}"
      DRP_KEY="{{.Param "dr-server/initial-password"}}"
      {{ else }}
      DRP_KEY="r0cketsk8ts"
      {{ end }}
      DRP_USER=""
      {{ if .ParamExists "dr-server/initial-user" }}
      DRP_USER="--remove-rocketskates --drp-user={{.Param "dr-server/initial-user"}}"
      DRP_KEY="{{.Param "dr-server/initial-user"}}:$DRP_KEY"
      {{ else }}
      DRP_KEY="rocketskates:$DRP_KEY"
      {{ end }}

      # Set up DRP ID
      {{ if .ParamExists "dr-server/install-drpid" }}
      DRPID={{ regexFind "[a-zA-Z0-9-]+" (.Param "dr-server/install-drpid") }}
      job_info "Using dr-server/install-drpid is $DRPID"
      {{ else }}
      if drpcli machines meta get $RS_UUID "dr-server/install-drpid" > /dev/null 2> /dev/null; then
        job_info "recovering DRPID from Meta.dr-server/install-drpid"
        DRPID=$(drpcli machines meta get $RS_UUID "dr-server/install-drpid" | jq -r .)
      else
        job_info "buiding DRPID from Name"
        DRPID={{ regexFind "[a-zA-Z0-9-]+" (list "drp" .Machine.Name | join "-") }}
      fi
      drpcli machines add $RS_UUID param "dr-server/install-drpid" to "$DRPID" > /dev/null
      job_info "dr-server/install-drpid not set... adding $DRPID"
      {{ end }}

      # Set up DRP license
      {{ if .ParamExists "dr-server/install-license" }}
      cat > rackn-license.json <<EOF
      {{ .ParamAsJSON "dr-server/install-license" }}
      EOF
      {{ else }}
      job_info "getting license file (will be copied to target)"
      drpcli contents show rackn-license > rackn-license.json

      # Verify DRP license
      job_info "  verify that endpoint $DRPID is in license $(cat rackn-license.json | jq .meta.Version)"
      epl=$(cat rackn-license.json | jq -r ".sections.profiles[\"rackn-license\"].Params[\"rackn/license-object\"].Endpoints | contains([\"$DRPID\"])")
      anymatch=$(cat rackn-license.json | jq -r '.sections.profiles["rackn-license"].Params["rackn/license-object"].Endpoints | contains(["MatchAny"])')
      KEY=$(cat rackn-license.json | jq -r '.sections.profiles["rackn-license"].Params["rackn/license"]')
      job_info "  endpoint $DRPID found? $epl.  MatchAny found? $anymatch"
      if [[ "$epl" == "true" || "$anymatch" == "true" ]] ; then
        job_info "  endpoint $DRPID found in license!"
      else
        job_info "  endpoint $DRPID missing, attempt to add to license"
        curl -X POST "https://cloudia.rackn.io/api/v1/license/update" \
          -H "Authorization: ${KEY}" \
          -H "rackn-endpointid: $DRPID" > rackn-license.json
      fi
      {{ end }}

      if [[ "$(cat rackn-license.json | jq 'has("sections")')" == "true" ]] ; then
        job_info "license valid"
      else
        job_error "license invalid"
        cat rackn-license.json
        exit 1
      fi

      # Do install
      job_info "==== DRP Server Install for ${DRPID} ===="

      # Check to see if this is an airgap install
      if [[ {{.ComposeParam "dr-server/install-airgap"}} == true ]] ; then
        job_info "Airgap install selected"
        # Download the airgap install script from the airgap-url param
        airgap_download_url={{ .ParamExpand "dr-server/airgap-file-url" }}
        if [[ -z "$airgap_download_url" ]]; then
          job_error "Invalid airgap download URL provided. Please ensure that the dr-server/airgap-file-url is set correctly with a valid URL."
          exit 1
        fi
        # Download the file to current location and then do the install
        res=$(curl -ksS $airgap_download_url -o install.sh 2>&1)
        res_exit_status=$?
        if [[ $res_exit_status -ne 0 ]]; then
          job_error "Failed to download the airgap install script."
          job_error "error response: $res"
          exit 1
        fi
      else
        # This is the regular install
        if drpcli files exists bootstrap/install.sh >/dev/null 2>/dev/null ; then
          job_info "downloading install.sh to cache (will be copied to target)"
          drpcli files download bootstrap/install.sh to install.sh >/dev/null
        else
          job_info "using files/bootstrap/install.sh (will be copied to target)"
          drp_cache "install.sh" "stable" "linux" "amd64" "http://get.rebar.digital/stable" "*"
        fi
      fi

      job_info "Stop dr-provision just in case"
      systemctl stop dr-provision || true

      chmod +x install.sh
      {{ if .Param "rs-debug-enable" }}
      DEBUG="--debug"
      {{ end }}
      {{ if .ComposeParam "dr-server/install-airgap" }}
      LOCAL_UI="--local-ui"
      {{ end }}
      ./install.sh --universal {{if .Param "dr-server/skip-port-check"}}--skip-port-check {{end}}--no-colors --drp-id=$DRPID $DRP_USER $DRP_PASSWORD --initial-contents="rackn-license.json" install $DEBUG --version={{.Param "dr-server/initial-version"}} --ipaddr={{.Machine.Address}} $LOCAL_UI

      job_info "Waiting for bootstrap to complete"

      ENDPOINT="https://{{ .Machine.Address }}:8092"
      DR_PASS={{ .Param "dr-server/initial-password" }}
      DR_USER={{ .Param "dr-server/initial-user" }}

      drpcli_raw -E "$ENDPOINT" -U $DR_USER -P $DR_PASS machines wait Name:$DRPID WorkflowComplete true >/dev/null

      job_success "Done"
Meta:
  type: "install"
  icon: "heart"
  color: "blue"
  title: "RackN Content"
  feature-flags: "sane-exit-codes"
