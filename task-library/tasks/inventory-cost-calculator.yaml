---
Name: inventory-cost-calculator
Description: "Attempts to calculate machine cost"
Documentation: |
  Stores cost estimate in `inventory/cost`

  For cloud costs (requires `cloud/provider`), will use the `cloud/instance-type` parameter
  and lookup cost models from `cloud/cost-lookup`

  If no specific type match is found, will use the approximate using the following formula: [RAM of machine in Gb]*[ram_multiple]*[fallback]

  At this time, no action for non-cloud machines

OptionalParams:
  - cloud/provider
  - cloud/instance-type
  - cloud/cost-lookup
Templates:
  - Name: "calculate-cost"
    Contents: |
      #!/bin/bash
      # RackN Copyright 2022

      {{template "setup.tmpl" .}}

      # if cluster create summary cost calculation
      if [[ "$RS_MC_ROLE" == "cluster" ]] ; then
        echo "sum all machines in the {{.Machine.Name}} cluster"
        COST=$(drpcli machines list --slim Param --params inventory/cost cluster/tags In {{.Machine.Name}} inventory/cost Gt 0 | jq -r '[.[].Params["inventory/cost"]] | add ')
        if [[ "$COST" == "null" ]] ; then
          echo "no inventory/cost information found (value was $COST)"
        else
          echo "store $COST result in cluster's inventory/cost param"
          drpcli machines set $RS_UUID param inventory/cost to $COST > /dev/null
        fi
        exit 0
      fi

      # if resource broker create summary cost calculation
      if [[ "$RS_MC_ROLE" == "resource-broker" ]] ; then
        echo "sum all machines managed by the {{.Machine.Name}} broker"
        COST=$(drpcli machines list --slim Param --params inventory/cost broker/name Eq {{.Machine.Name}} inventory/cost Gt 0 | jq -r '[.[].Params["inventory/cost"]] | add ')
        if [[ "$COST" == "null" ]] ; then
          echo "no inventory/cost information found (value was $COST)"
        else
          echo "store $COST result in cluster's inventory/cost param"
          drpcli machines set $RS_UUID param inventory/cost to $COST > /dev/null
        fi
        exit 0
      fi

      {{ if not (empty .Machine.Context) }}
      echo "No Action: does not run in a Machine Context."
      exit 0
      {{ end }}

      {{ if and (.ParamExists "cloud/provider") (.ParamExists "cloud/instance-type") }}
      {{   $cloud := (.ParamCompose "cloud/provider") }}
      {{   $type := (.Param "cloud/instance-type") }}
      {{   $ram := (get (.Param "cloud/cost-lookup") "ram_multiple") }}
      echo "Looking for {{$cloud}} costs for cloud/instance-type {{$type}}"
      {{   if hasKey (.Param "cloud/cost-lookup") $cloud }}
      echo "  found entries for {{$cloud}} in cloud/cost-lookup"
      {{     $costs := (get (.Param "cloud/cost-lookup") $cloud) }}
      {{     if hasKey $costs $type }}
      COST={{get $costs $type}}
      echo "  {{$type}} entry is $COST"
      {{     else }}
      {{       if .ParamExists "inventory/RAM" }}
      echo "  using RAM approximation and fallback"
      COST={{mulf (get $costs "fallback") $ram (.Param "inventory/RAM") }}
      {{       else }}
      COST={{get $costs "fallback"}}
      {{       end }}
      echo "  no {{$type}} found, using fallback of $COST"
      {{     end }}
      {{   else }}
      {{     if .ParamExists "inventory/RAM" }}
      COST={{mulf (get (.Param "cloud/cost-lookup") "fallback.fallback") $ram (.Param "inventory/RAM")}}
      {{     else }}
      COST={{get (.Param "cloud/cost-lookup") "fallback.fallback"}}
      {{     end }}
      {{   end }}
      echo "  setting inventory/cost to $COST"
      drpcli machines set $RS_UUID param inventory/cost to $COST > /dev/null

      {{ else }}
      echo "No Action: in the future, add calculation for cloud/provider and cloud/instance-type"
      {{ end }}

      echo "done"
      exit 0
Meta:
  color: "olive"
  icon: "money bill alternate"
  title: "RackN Content"
