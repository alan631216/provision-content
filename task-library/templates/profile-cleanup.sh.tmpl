#!/usr/bin/env bash
# remove profiles from a machine object
# Copyright, RackN 2022

###
#  Remove profiles from a Machine object specified by wildcard
#  pattern matches in 'profile-cleanup-selection-pattern'.  Please
#  see the Param Documentation field for specifics of pattern matching.
###

{{ template "setup.tmpl" . }}

{{ if ( .Param "profile-cleanup-selection-pattern" ) -}}
echo ">>> Starting profile cleanup on machine $RS_UUID ('{{ .Machine.Name }}')"
{{ else -}}
echo ">>> No 'profile-cleanup-selection-pattern' specified. Exiting."
exit 0
{{ end -}}

{{ $patterns := ( .Param "profile-cleanup-selection-pattern" ) -}}
echo ">>> Cleanup pattern(s) being used:"
{{ range $pattern := $patterns -}}
echo "    '{{ $pattern }}'"
{{ end -}}

{{ range $pattern := $patterns -}}
echo "+++ Processing profile match for pattern '{{ $pattern }}'"
# assign to array PROFILES
PROFILES+=( $(drpcli machines show $RS_UUID --slim params | jq -r '.Profiles[] | select(test("{{ $pattern }}"))') )
{{ end -}}

echo "+++ Found profiles for cleanup:"
for PROFILE in ${PROFILES[@]}
do
  echo "    $PROFILE"
done

{{ if eq ( .Param "profile-cleanup-skip" ) true -}}
echo ">>> Cleanup has been disabled by Param option.  Skipping cleanup."
echo "    ('profile-cleanup-skip' is set to 'true')"
exit 0
{{ end -}}

echo ""

for PROFILE in ${PROFILES[@]}
do
  echo "+++ Removing Profile: $PROFILE"
  drpcli machines removeprofile $RS_UUID $PROFILE > /dev/null
done

echo ""
echo ">>> Profiles that remain on machine after cleanup:"
drpcli machines show $RS_UUID --slim params | jq -r '.Profiles[]'
echo ""

