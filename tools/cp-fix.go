package main

import (
	"fmt"
	"gitlab.com/rackn/provision/v4/store"
	"os"
)

func main() {
	d := store.Directory{Path: "."}
	if err := d.Open(store.YamlCodec); err != nil {
		fmt.Printf("Failed open: %v\n", err)
		os.Exit(1)
	}
	res := d.MetaData()
	err := d.SetMetaData(res)
	if err != nil {
		fmt.Printf("Failed store: %v\n", err)
		os.Exit(1)
	}
}
