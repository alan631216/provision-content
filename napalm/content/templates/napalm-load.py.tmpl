###
#  Simple NAPALM driver script for EOS to merge or replace
#  config based on a config file.
#
#  usage:  load.py [ merge | replace ] config_file
###

from __future__ import print_function

import napalm
import sys
import os

def main(operation, config_file):
  """Load or Merge a config for the device."""

  if not (os.path.exists(config_file) and os.path.isfile(config_file)):
    msg = "Missing or invalid config file {0}".format(config_file)
    raise ValueError(msg)

  print("Loading merge file {0}.".format(config_file))

  # Use the appropriate network driver to connect to the device:
  driver = napalm.get_network_driver("{{ .ParamCompose "napalm/config-driver" }}")

  # Connect:
  device = driver(
    hostname="{{ .ParamCompose "napalm/config-hostname" }}",
    username="{{ .ParamCompose "napalm/config-username" }}",
    password="{{ .Param "napalm/config-password" }}",
    {{ if .Param "napalm/config-optional" }}optional_args={{ .ParamCompose "napalm/config-optional" }},{{ end }}
    {{ .ParamCompose "napalm/config-extra" }}
  )

  print("Opening ...")
  device.open()

  print("Loading candidate ...")

  if operation == 'replace':
    device.load_replace_candidate(filename=config_file)
  elif operation == 'merge':
    device.load_merge_candidate(filename=config_file)
  else:
    print('FATAL: invalid operation "' + operation + '"; expected "replace" or "merge".')
    sys.exit(1)

  # Note that the changes have not been applied yet. Before applying
  # the configuration you can check the changes:
  print("============================= compare config =============================")
  print(device.compare_config())

  print("")
  print("============================= {{ .Param "napalm/load-operation" }} config =============================")
{{- if .Param "napalm/commit-config" }}
  device.commit_config()
{{- else }}
  print("Discarding changes... (set 'napalm/commit-config' to 'true' to make changes)")
  device.discard_config()
{{- end }}

  print("{{ .Param "napalm/load-operation" }} operation completed")
  print("")
  print("============================= close device =============================")
  # close the session with the device.
  device.close()
  print("Done.")

if __name__ == "__main__":
  if len(sys.argv) < 3:
    print('Invalid usage.')
    print('Usage: load.py [ merge | replace ] config_file')
    sys.exit(1)
  operation = sys.argv[1]
  config_file = sys.argv[2]
  main(operation, config_file)

