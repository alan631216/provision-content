---
Name: proxmox-debian-installer
Description: Installs Proxmod on top of Debian 10 (Buster)
Documentation: |
  This task sets up and installs latest stable Proxmox VE on
  top of an already installed Debian 10 (Buster) system.  This
  can be run betweent the `finsish-install` and `complete`
  stage of the RackN provided `debian-base`` workflow.

  This is also used in the `proxmox-debian-installer` Workflow
  which installs Debian 10 (Buster) first.

Meta:
  color: orange
  feature-flags: sane-exit-codes
  icon: expand arrows alternate
  title: RackN Content
RequiredParams: []
OptionalParams: []
Prerequisites: []
Templates:
  - Name: proxmox-debian-fix-etc_hosts.sh.tmpl
    Contents: |
      #!/usr/bin/env bash
      # Fixup /etc/hosts entries in system prior to installation of Proxmox

      {{ template "setup.tmpl" . }}

      ###
      #  PROXMOX is extremely intolerant of hostname changes ... basically, you can't ...
      #  and you must have the hostname in /etc/hosts with the IP address of the system ...
      #  there are LOTS OF WAYS this could all blow up because it's so very sensitive, trying
      #  to account for as many as we can - please report problems with package installations in
      #  this task to 'support@rackn.com' if you have issues, likely /etc/hosts problems
      ###

      echo ">>> if the hostname is set correctly already, move on"
      if [[ $( hostname --ip-address ) != "127.0.1.1" ]]; then
        echo ">>> 'hostname --ip-address' test passed, so we _think_ the hostname is set correctly"
        echo 0
      fi

      ADDR="{{.Machine.Address}}"
      N_HOST="{{.Machine.ShortName}}"
      N_FQDN="{{.Machine.Name}}"
      STAT_HOST=$(getent hosts "$N_HOST" | grep -v "^127.0.0.1" | grep -v "^127.0.1.1" || true)
      STAT_FQDN=$(getent hosts "$N_FQDN" || true)
      STAT_ADDR=$(getent hosts "$ADDR" || true)

      if [[ -n "$STAT_HOST" || -n "$STAT_FQDN" || -n "$STAT_ADDR" ]]
      then
        echo ">>> 'getent hosts' test passed, so  we _think_ /etc/hosts records are correct"
      else
        [[ -z "$ADDR" || "$ADDR" == "<nil>" ]] && xiterr 1 "'.Machine.Adddress' returned empty or '<nil>' record" || true
        [[ "$N_HOST" == "$N_FQDN" ]] && NAME="$N_HOST" || NAME="$N_HOST $N_FQDN"

        echo ">>> add the machines IP address/name in /etc/hosts"
        # inserts one line after the localhost entry
        sed -i.bak1 "/^127\.0\.0\.1.*/a $ADDR  $NAME" /etc/hosts
      fi

      echo ">>> Force commenting out 127.0.1.1 entry if it exists"
      sed -i.bak2 's/^\(127.0.1.1.*$\)/#\1/g' /etc/hosts


      if ! grep -qE "^127\.0\.0\.1\s+localhost\s+localhost\.localdomain$" /etc/hosts; then
        echo ">>> Force fixing localhost entry for correct Proxmox use"
        sed -i.bak3 "/^127\.0\.0\.1.*/a 127.0.0.1 localhost localhost.localdomain" /etc/hosts
      fi

      echo ">>> The /etc/hosts file now contains:"
      cat /etc/hosts

      echo ">>> verify via command hostname it's set correctly"
      hostname --ip-address

  - Name: proxmox-debian-install.sh.tmpl
    Contents: |
      #!/usr/bin/env bash
      # Install Proxmox on the system.

      ###
      #  This install script is loosely based on the Proxmox described install
      #  process, which can be found at:
      #
      #    * https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_Buster
      ###

      {{ template "setup.tmpl" . }}

      {{ if eq ( .Param "proxmox/kernel-reboot-requested" ) true }}
      echo ">>> System requested reboot from stripping the old kernels, cleanly exiting now." 
      drpcli params remove $RS_UUID param proxmox/kernel-reboot-requested > /dev/null 2>&1
      exit 0
      {{ end -}}

      # get the packages selected for installation, extra space is intentionally
      # injected here for grep pattern checks below
      #
      PKGS=' {{ .Param "proxmox/package-selections" }} '

      KEY_BASE="/etc/apt/trusted.gpg.d"

      M=$(uname -m)
      case $M in
        x86_64|amd64) ARCH="[arch=amd64]" ;;
        *)            ARCH="[arch=$M]"    ;;
      esac

      export $(grep ^VERSION_CODENAME= /etc/os-release)
      echo ">>> add proxmox no-subscription repos"
      echo "deb $ARCH http://download.proxmox.com/debian/pve $VERSION_CODENAME pve-no-subscription" > /etc/apt/sources.list.d/pve-install-repo.list

      printf ">>> get the GPG key(s) for the repo ... "

      {{ range $key, $val := ( .Param "proxmox/gpg-keys" ) -}}
      {{ $file := base $val }}
      echo "GPG key: '{{ $file }}'"
      wget "{{ $val }}" -O $KEY_BASE/{{ $file }}
      {{ end -}}

      echo ">>> bump everything up to current versions"
      apt -y update && apt -y full-upgrade

      echo ">>> preseed the samba and postfix package questions"
      debconf-set-selections /root/proxmox-debconf-set-selections

      echo ">>> install proxmox with these package selections: $PKGS"
      apt -y install $PKGS

      {{ if .Param "proxmox/strip-kernel" -}}
      # exit and force a reboot before the next task so we are on a
      # clean kernel before (optionally) ripping the old ones off
      job_warn "Rebooting to cleanly strip old kernels off the system"
      echo ">>> Setting 'proxmox/kernel-reboot-requested' to break reboot loop cycle."
      drpcli machines add $RS_UUID param proxmox/kernel-reboot-requested to true
      exit_reboot
      {{ else -}}
      job_info "Not rebooting; no kernels designated to be stripped ('proxmox/strip-kernel')"
      {{ end -}}

  - Name: proxmox-debian-cleanup.sh.tmpl
    Contents: |
      #!/usr/bin/env bash
      # post reboot of proxmox kernel install, strip old kernels and other
      # packages as recommended

      {{ template "setup.tmpl" . }}

      job_info "Starting cleanup tasks (package and kernels removed, etc.)"

      # recommended by install guide - see URL above for reference
      echo ">>> remove the OS prober package"
      apt -y remove os-prober

      {{ if .Param "proxmox/strip-kernel" -}}
      echo ">>> strip the Debian stock kernel packages"
      for STRIP in {{ ( .Param "proxmox/strip-kernel-packages" ) }}
      do
        # apt sucks hard ...
        if [[ $(apt list "$STRIP" 2> /dev/null  | wc -l) -gt 1 ]]
        then
          GRUB_ME="true"
          echo ">>> Strip packages matching: '$STRIP'"
          apt -y remove "$STRIP"
        else
          echo "Skipping strip pattern (no matches):  '$STRIP'"
        fi
      done

      if [[ -n "$GRUB_ME" ]]
      then
        echo ">>> Updating the grub bootloader"
        update-grub
      else
        echo "--- Not updating grub, no package patterns matched for removal"
      fi
      {{ else -}}
      job_info "'proxmox/strip-kernel' is 'false', no kernel package removal performed"
      {{ end -}}

      echo ">>> Handling APT repo for PVE Enterprise mirror (requires Licensing from Proxmox)"
      {{ if .Param "proxmox/pve-enterprise-repo-enable" -}}
      echo '!!! need to add handling of the PVE Entperrise repo here - nothing done'
      {{ else -}}
      ENT="/etc/apt/sources.list.d/pve-enterprise.list"
      [[ -r "$ENT" ]] && { echo "--- Removing injected enterprise repo."; rm -f "$ENT"; } || echo "+++ No enterprise repo list found."
      {{ end -}}
