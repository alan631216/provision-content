---
Name: "sledgehammer-install-centos-7"
Description: "Install bootenv for building Sledgehammer images based on CentOS 7"
Documentation: |
  This BootEnv is used as the basis for building Sledgehammer, our in-memory discovery
  and inventory management environment.  This bootenv is reponsible for performing
  a basic CentOS install on a sacrificial machine.  The tasks that run after the install
  has finished are responsible for stripping out everything we do not need for Sledgehammer
  to boot as an in-memory OS image and packaging everything up for distribution.
Loaders:
  amd64-uefi: EFI/BOOT/BOOTX64.EFI
  arm64-uefi: EFI/BOOT/BOOTAA64.EFI
OS:
  Family: "redhat"
  Version: "7"
  Name: "centos-7"
  SupportedArchitectures:
    x86_64:
      IsoFile: "CentOS-7-x86_64-Minimal-2009.iso"
      IsoUrl: "https://rackn-repo.s3.amazonaws.com/isos/centos/7/CentOS-7-x86_64-Minimal-2009.iso"
      Sha256: "07b94e6b1a0b0260b94c83d6bb76b26bf7a310dc78d7a9c7432809fb9bc6194a"
      Kernel: "images/pxeboot/vmlinuz"
      Initrds:
        - "images/pxeboot/initrd.img"
      BootParams: >-
        ksdevice=bootif
        ks={{.Machine.Url}}/sledgehammer.ks
        method={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
    aarch64:
      IsoFile: "CentOS-7-aarch64-Minimal-2009.iso"
      Sha256: "1bef71329e51f9bed12349aa026b3fe0c4bb27db729399a3f9addae22848da9b"
      IsoUrl: "https://rackn-repo.s3.amazonaws.com/isos/centos/7/CentOS-7-aarch64-Minimal-2009.iso"
      Kernel: "images/pxeboot/vmlinuz"
      Initrds:
        - "images/pxeboot/initrd.img"
      BootParams: >-
        ksdevice=bootif
        ks={{.Machine.Url}}/sledgehammer.ks
        method={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
Templates:
  - ID: "default-pxelinux.tmpl"
    Name: "pxelinux"
    Path: "pxelinux.cfg/{{.Machine.HexAddress}}"
  - ID: "default-ipxe.tmpl"
    Name: "ipxe"
    Path: "{{.Machine.Address}}.ipxe"
  - ID: "default-pxelinux.tmpl"
    Name: "pxelinux-mac"
    Path: 'pxelinux.cfg/{{.Machine.MacAddr "pxelinux"}}'
  - ID: "default-ipxe.tmpl"
    Name: "ipxe-mac"
    Path: '{{.Machine.MacAddr "ipxe"}}.ipxe'
  - ID: "default-grub.tmpl"
    Name: "grub"
    Path: "grub/{{.Machine.Address}}.cfg"
  - ID: "default-grub.tmpl"
    Name: "grub-mac"
    Path: 'grub/{{.Machine.MacAddr "grub"}}.cfg'
  - ID: default-grub.tmpl
    Name: grub-secure
    Path: '{{.Env.PathFor "tftp" ""}}/EFI/BOOT/grub.cfg'
  - Name: "sledgehammer.ks"
    Path: "{{.Machine.Path}}/sledgehammer.ks"
    Contents: |
      lang en_US.UTF-8
      keyboard us
      timezone UTC
      auth --useshadow --enablemd5
      # rebar1
      rootpw --iscrypted $1$UwJdGUMy$ORqjDQIW//wt7sWY.xG9M0
      selinux --permissive
      firewall --disabled
      {{range .MachineRepos}}
      {{.Install}}
      {{end}}
      bootloader --location=mbr
      zerombr
      clearpart --all
      part /boot --fstype ext4 --size=512
      part /boot/efi --fstype vfat --size=512
      part / --fstype ext4 --size=1 --grow
      text
      poweroff
      %packages
      OpenIPMI
      OpenIPMI-tools
      aic94xx-firmware
      audit
      authconfig
      banner
      basesystem
      bash
      bsdtar
      bzip2
      coreutils
      cpio
      curl
      dhclient
      dmidecode
      dosfstools
      e2fsprogs
      efibootmgr
      file
      filesystem
      firewalld
      fuse
      fuse-libs
      fuse-ntfs-3g
      gdisk
      glibc
      gzip
      hostname
      initscripts
      iproute
      iprutils
      iptables
      iputils
      jq
      kbd
      kernel
      kernel-tools
      kexec-tools
      less
      libsysfs
      linux-firmware
      lldpd
      lshw
      lvm2
      man-db
      mdadm
      {{if (eq .Machine.Arch "amd64")}}
      grub2-efi-x64
      shim-x64
      microcode_ctl
      {{end}}
      mktemp
      ncurses
      nfs-utils
      ntfs-3g
      ntfsprogs
      ntp
      openssh-clients
      openssh-server
      openssl-libs
      parted
      passwd
      pciutils
      policycoreutils
      procps-ng
      rootfiles
      rpm
      rsyslog
      selinux-policy-minimum
      setup
      shadow-utils
      squashfs-tools
      stress
      stress-ng
      sudo
      systemd
      systemd-resolved
      systemd-networkd
      tar
      tpm2-tools
      unzip
      util-linux
      vim-enhanced
      wget
      which
      xorriso
      xfsdump
      xfsprogs
      xz
      yum
      zlib
      %end

      %pre
      {{ range $intf := .Param "sledgehammer-7/extra-ifs" }}
      dhclient --no-pid {{ $intf }}
      {{end}}
      %end

      %post
      cat >/etc/selinux/config <<EOF
      SELINUX=permissive
      SELINUXTYPE=minimum
      EOF
      {{template "reset-workflow.tmpl" .}}
      {{template "runner.tmpl" .}}
      %end
