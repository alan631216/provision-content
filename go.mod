module gitlab.com/rackn/provision-content

go 1.16

require (
	github.com/itchyny/gojq v0.12.13
	gitlab.com/rackn/provision/v4 v4.13.0
)
