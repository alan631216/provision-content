---
Name: hpe-firmware-flash-list
Description: HPE Firmware Flash From List
RequiredParams:
  - skip-flash
  - flash-list
Templates:
  - Name: hpe-firmware-flash-list
    Contents: |
      #!/usr/bin/env bash

      {{ template "setup.tmpl" . }}
      {{ template "set-proxy-servers.sh.tmpl" . }}
      {{ template "hpe-ilorest-helper.sh.tmpl" . }}

      function clean_curl() {
          local _src=$1
          local _dest=$2

          echo "Downloading $_src"
          set +e
          curl -L -f -k -o $_dest $_src
          set -e
          if [[ $? != 0 ]] ; then
              xiterr 1 "Failed to download $_dest from $_src"
          fi
      }

      skip_flash="{{.Param "skip-flash"}}"
      if [[ $skip_flash = true ]]; then
          echo "Skipping all flash operations due to skip-flash being true"
          exit 0
      fi

      if ! grep -q 'sledgehammer\.iso' /proc/cmdline; then
          echo "System not in Sledgehammer, exiting"
          exit 0
      fi

      want_reboot=no
      failed=no
      {{ range $index, $elem := .Param "flash-list" }}
        {{ if not (has $elem.File ($.Param "flash-list-check-list")) }}
        FILENAME="file.{{$index}}"
        {{ if $elem.Force }}
        FORCE="-f"
        FORCE_G="-g"
        {{ else }}
        FORCE=
        FORCE_G=
        {{ end }}
        {{ if $.Param "flash-list-force" }}
        FORCE="-f"
        FORCE_G="-g"
        {{ end }}

        {{ if hasPrefix "http" $elem.File }}
        clean_curl "{{$elem.File}}" "$FILENAME"
        {{ else }}
        clean_curl "{{$.ProvisionerURL}}/{{$elem.File}}" "$FILENAME"
        {{ end }}

        lfailed=no

        {{ if eq $elem.Type "rpm" }}
        # Check to see if RPM is already installed.
        set +e
        RPMNAME=$(basename {{$elem.File}} | sed "s/.rpm//")
        if ! rpm -q ${RPMNAME} 2>/dev/null >/dev/null ; then
          rpm -ivh $FILENAME
        fi
        # Find SETUP command
        SETUP=$(rpm -ql $RPMNAME | grep '/setup$')
        if [[ "$SETUP" = "" ]] ; then
          SETUP=$(rpm -ql $RPMNAME | grep '/hpsetup$')
        fi
        if [[ "$SETUP" = "" ]] ; then
          echo "Failed to find a setup script!"
          echo "Files in $RPMNAME"
          rpm -ql $RPMNAME
          exit 1
        fi
        set -e
        # One day make this an argument
        TPMARG=""
        if [[ "$RPMNAME" =~ ^firmware-system ]] ; then
          TPMARG="--tpmbypass"
        fi
        if [[ "$RPMNAME" =~ ^firmware-ilo ]] ; then
          TPMARG="--tpmbypass"
        fi
        cd $(dirname $SETUP)
        set -o pipefail
        if [[ "$RPMNAME" =~ ^firmware-nic-mellanox ]] ; then
          set +e
          $SETUP -s ${FORCE} |& tee $FILENAME.log
          case $? in
            0)
              if grep -iq "or reboot machine" $FILENAME.log ; then
                echo "Installed successfully - reboot required"
                want_reboot=yes
              else
                echo "Installed successfully - no reboot required"
              fi
              ;;
            1)
              echo "Nothing to do";;
            *)
              echo "$SETUP returned $?"
              lfailed=yes;;
          esac
          set -e
        else
          set +e
          $SETUP -s ${FORCE_G} ${TPMARG} ${FORCE} |& tee $FILENAME.log
          case $? in
            0)
              echo "Installed successfully - no reboot required";;
            1)
              echo "Installed successfully - reboot required"
              want_reboot=yes;;
            2)
              echo "Nothing to do - at current level";;
            3)
              echo "Nothing to do - current level or no matching hardware";;
            4)
              echo "Nothing to do - matching hardware doesn't have firmware";;
            5)
              echo "Nothing to do - user interrupted the install";;
            6)
              echo "Nothing to do - hardware disabled";;
            7)
              echo "A failure has occurred."
              lfailed=yes;;
            *)
              echo "$SETUP returned $?"
              lfailed=yes;;
          esac
          set -e
        fi
        set +o pipefail
        cd -
        {{ else }}
        {{ if eq $elem.Type "fwpkg" }}

        echo "Getting current inventory data"
        set +e
        safe_ilorest list Name Version Oem/Hpe/DeviceClass Oem/Hpe/DeviceContext Oem/Hpe/Targets --selector=SoftwareInventory --json > i_tmp.json
        RC=$?
        case $RC in
          23)
            echo "ILO reports 23?!?!?! This requires reboot to address."
            exit_incomplete_reboot
            ;;
          0)
            echo "inventory retrieved"
            ;;
          *)
            echo "Inventory failed: $RC"
            exit 1
            ;;
        esac
        # ILO Rest can add lines to the json that aren't correct
        grep -v "^Discover" i_tmp.json > inventory.json
        set -e

        echo "Getting id/version lists from fwpkg"
        rm -rf tmpdir
        mkdir tmpdir
        cd tmpdir
        unzip ../${FILENAME}
        jq '[{"DC": .DeviceClass, "Pieces": (.Devices.Device[]|{"Target": .Target, "Version": .Version, "Name": .DeviceName, "Reset": .FirmwareImages[].ResetRequired})}]' payload.json > ../comp.json
        cd ..

        echo "Set the present and install flags"
        present=false
        install=false
        why="unknown"

        while read line
        do
          DC=$(echo $line | awk -F'|' '{ print $1 }')
          TARGET=$(echo $line | awk -F'|' '{ print $2 }')
          VERSION=$(echo $line | awk -F'|' '{ print $3 }')
          VERSION=$(echo $VERSION|sed 's/^U[0-9]*[ \t]*//')
          RESET=$(echo $line | awk -F'|' '{ print $4 }')

          echo -n "Processing: $DC $TARGET $VERSION $RESET "
          count=$(jq ".|map(select((((.Oem.Hpe.DeviceClass|tostring)==\"$DC\") or ((.Oem.Hpe.DeviceClass|tostring)==\"null\")) and ([]+.Oem.Hpe.Targets|contains([\"$TARGET\"]))))|length" inventory.json)

          if [[ $count != 0 ]] ; then
            present=true
            CURVERSION=$(jq ".|map(select((((.Oem.Hpe.DeviceClass|tostring)==\"$DC\") or ((.Oem.Hpe.DeviceClass|tostring)==\"null\")) and ([]+.Oem.Hpe.Targets|contains([\"$TARGET\"]))))[0].Version" -r inventory.json)
            CURVERSION=$(echo $CURVERSION|sed 's/^U[0-9]*[ \t]*//')
            CURVERSION=$(echo $CURVERSION|sed 's/^A[0-9]*[ \t]*//')

            MYIFS=$IFS
            IFS='-._ ' read -ra new_ver <<< "$VERSION"
            IFS='-._ ' read -ra old_ver <<< "$CURVERSION"
            IFS=$MYIFS

            NL=${#new_ver[*]}
            OL=${#old_ver[*]}
            if (( $NL > $OL )) ; then
              CL=$OL
            else
              CL=$NL
            fi
            install=false
            why="Same value"
            for ((i=0; i<$CL; i++)) ; do
              bnv=${new_ver[$i]}
              if [[ $bnv =~ ^v ]] ; then
                bnv="${bnv:1}"
              fi
              bov=${old_ver[$i]}
              if [[ $bov =~ ^v ]] ; then
                bov="${bov:1}"
              fi
              nv=$(printf "%d" ${bnv} 2>/dev/null || echo "not valid")
              ov=$(printf "%d" ${bov} 2>/dev/null || echo "not valid")

              if [[ "$nv" == "0not valid" || "$ov" == "0not valid" ]] ; then
                if [[ "$bnv" != "$bov" ]] ; then
                  install=false
                  why="Not the same platform"
                  break
                fi
                continue
              fi
              if (( $nv < $ov )) ; then
                install=false
                why="Current version newer than package"
                break
              fi
              if (( $nv > $ov )) ; then
                install=true
                why="Package newer than current version"
                break
              fi
            done
          fi

          if [[ "$install" == "true" ]] ; then
            break
          fi
        done <<< $(jq -r '.[]|.DC + "|" + .Pieces.Target + "|" + .Pieces.Version + "|" + (.Pieces.Reset|tostring)' comp.json)

        if [[ "$present" == "false" ]] ; then
          echo "No component present for {{$elem.File}}."
        elif [[ "$install" == "false" ]]  ; then
          echo "Not installing the {{$elem.File}}: $why"
        else
          rm -rf inst.{{$index}}.fwpkg
          cp $FILENAME inst.{{$index}}.fwpkg
          echo "Updating {{$elem.File}}: $why"
          set -o pipefail
          set +e
          safe_ilorest flashfwpkg inst.{{$index}}.fwpkg |& tee flash.out
          case $? in
            0)
              if grep -iq "upload failed" flash.out ; then
                echo "Component is missing??? - skipping"
              else
                if grep -iq "reboot is required" flash.out ; then
                  want_reboot=yes
                fi
                # Loop over parameter that means success needs reboot.
                {{ range $_, $name := $.ParamExpand "flash/hpe-success-reboot-list" }}
                if [[ "{{$elem.File}}" =~ "{{$name}}" ]] ; then
                  echo "Filename {{$elem.File}} matches {{$name}} - signal reboot"
                  want_reboot=yes
                fi
                {{ end }}
              fi
              ;;
            *)
              lfailed=yes
              echo "Failed to flash with (inst.{{$index}}.fwpkg) $FILENAME - {{$elem.File}}"
              ;;
          esac
          set +o pipefail
          set -e
        fi
        {{ end }}
        {{ end }}

        if [[ $lfailed = no ]] ; then
          drpcli machines get $RS_UUID param flash-list-check-list | jq ' (.+ ["{{$elem.File}}"] | unique)' | drpcli machines set $RS_UUID param flash-list-check-list to -
        fi
        if [[ $lfailed = yes ]] ; then
          failed=yes
        fi
        {{ else }}
        echo "Image {{$elem.File}} is already installed.  To reinstall, clear the 'flash-list-installed-list'"
        {{ end }}
      {{ end }}

      if [[ $want_reboot = yes ]]; then
          echo "Need reboot - start rebooting"
          exit_incomplete_reboot
      fi

      if [[ $failed = yes ]]; then
          echo "Something failed - error"
          exit 1
      fi

      {{ template "flash-list-installed.sh.tmpl" . }}

      echo "Nothing else to do and complete"
      drpcli machines set "$RS_UUID" param skip-flash to true
      exit 0
