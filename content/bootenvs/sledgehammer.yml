---
# Sledgehammer (the per machine bootenv)
# This bootenv requires the start-up.sh file to be rendered by the discovery bootenv.
# These two bootenvs are linked and should be used as a pair.
Name: "sledgehammer"
Description: "Ram-Only image loaded with tools to allow for discovery and maintenance"
Documentation: |
  The Sledgehammer BootEnv is used in conjunction with Discovery to boot a
  machine in to an in-memory (RAM only) operating system.  The Machine will
  be enrolled in the DRP Endpoint via Sledgehammer.

  Many maintenance and hardware related workflows require to be run from the
  Sledgehammer BootEnv.

  Note: see `start` stage for information about joining machines with an
  installed O/S into Digital Rebar.
Meta:
  type: "sledgehammer"
  feature-flags: "change-stage-v2"
  icon: "microchip"
  color: "green"
  title: "Digital Rebar Community Content"
  group-by: Discovery
Loaders:
  amd64-uefi: 97064f6a618603ffd6f37670dafa81b41a00b5cc/shimx64.efi
OS:
  Family: "redhat"
  Name: "sledgehammer"
  SupportedArchitectures:
    amd64:
      IsoFile: "sledgehammer-97064f6a618603ffd6f37670dafa81b41a00b5cc.amd64.tar"
      IsoUrl: "http://rackn-sledgehammer.s3-website-us-west-2.amazonaws.com/sledgehammer/97064f6a618603ffd6f37670dafa81b41a00b5cc/sledgehammer-97064f6a618603ffd6f37670dafa81b41a00b5cc.amd64.tar"
      Kernel: "97064f6a618603ffd6f37670dafa81b41a00b5cc/vmlinuz0"
      Initrds:
        - "97064f6a618603ffd6f37670dafa81b41a00b5cc/stage1.img"
      BootParams: >-
        rootflags=loop
        root=live:/sledgehammer.iso
        rootfstype=auto
        ro
        liveimg
        rd_NO_LUKS
        rd_NO_MD
        rd_NO_DM
        provisioner.web={{.ProvisionerURL}}
        {{ if .ParamExists "network-data" }}
        {{ $nd := .ComposeParam "network-data" }}
        {{ if hasKey $nd "prov" }}
        {{ $prov := get $nd "prov" }}
        {{ if and (hasKey $prov "address") (hasKey $prov "netmask") }}
        provisioner.ip={{ get $prov "address" }}/{{ .NetmaskToCIDR (get $prov "netmask") }}
        {{ end }}
        {{ if hasKey $prov "gateway" }}
        provisioner.gw={{ get $prov "gateway" }}
        {{ end }}
        {{ if hasKey $prov "interface" }}
        provisioner.interface={{ get $prov "interface" }}
        {{ end }}
        {{ if hasKey $prov "vlan" }}
        provisioner.vlan={{ get $prov "vlan" }}
        {{ end }}
        {{ if hasKey $prov "mtu" }}
        provisioner.mtu={{ get $prov "mtu" }}
        {{ end }}
        {{ end }}
        {{ end }}
        {{ if .ParamExists "dns-servers" }}
        provisioner.dns={{ first (.Param "dns-servers") }}
        {{ end }}
        {{.Param "kernel-options"}}
        --
        {{.Param "kernel-console"}}
    arm64:
      Loader: "grubarm64.efi"
      IsoFile: "sledgehammer-9b5276ac5826520829aa73c149fe672fe2363656.arm64.tar"
      IsoUrl: "https://s3.us-east-2.amazonaws.com/vl-hammer/sledgehammer-9b5276ac5826520829aa73c149fe672fe2363656.arm64.tar"
      Kernel: "9b5276ac5826520829aa73c149fe672fe2363656/vmlinuz0"
      Initrds:
        - "9b5276ac5826520829aa73c149fe672fe2363656/stage1.img"
      BootParams: >-
        rootflags=loop
        root=live:/sledgehammer.iso
        rootfstype=auto
        ro
        liveimg
        rd_NO_LUKS
        rd_NO_MD
        rd_NO_DM
        provisioner.web={{.ProvisionerURL}}
        rs.uuid={{.Machine.UUID}}
        {{ if .ParamExists "network-data" }}
        {{ $nd := .ComposeParam "network-data" }}
        {{ if hasKey $nd "prov" }}
        {{ $prov := get $nd "prov" }}
        {{ if and (hasKey $prov "address") (hasKey $prov "netmask") }}
        provisioner.ip={{ get $prov "address" }}/{{ .NetmaskToCIDR (get $prov "netmask") }}
        {{ end }}
        {{ if hasKey $prov "gateway" }}
        provisioner.gw={{ get $prov "gateway" }}
        {{ end }}
        {{ if hasKey $prov "interface" }}
        provisioner.interface={{ get $prov "interface" }}
        {{ end }}
        {{ if hasKey $prov "vlan" }}
        provisioner.vlan={{ get $prov "vlan" }}
        {{ end }}
        {{ if hasKey $prov "mtu" }}
        provisioner.mtu={{ get $prov "mtu" }}
        {{ end }}
        {{ end }}
        {{ end }}
        {{ if .ParamExists "dns-servers" }}
        provisioner.dns={{ first (.Param "dns-servers") }}
        {{ end }}
        {{.Param "kernel-options"}}
        --
        {{.Param "kernel-console"}}
    rpi4:
      Loader: "grubarm64.efi"
      IsoFile: "sledgehammer-d50c0910deee78591225ef5ede200021b7288b5b.rpi4.tar"
      IsoUrl: "https://rackn-sledgehammer.s3-us-west-2.amazonaws.com/sledgehammer/d50c0910deee78591225ef5ede200021b7288b5b/sledgehammer-d50c0910deee78591225ef5ede200021b7288b5b.rpi4.tar"
      Kernel: "rpi-5.4.35/kernel8.img"
      Initrds:
        - "rpi-5.4.35/stage1.img"
      BootParams: >-
        rootflags=loop
        root=live:/sledgehammer.iso
        rootfstype=auto
        ro
        liveimg
        rd_NO_LUKS
        rd_NO_MD
        rd_NO_DM
        provisioner.web={{.ProvisionerURL}}
        rs.uuid={{.Machine.UUID}}
        {{ if .ParamExists "network-data" }}
        {{ $nd := .ComposeParam "network-data" }}
        {{ if hasKey $nd "prov" }}
        {{ $prov := get $nd "prov" }}
        {{ if and (hasKey $prov "address") (hasKey $prov "netmask") }}
        provisioner.ip={{ get $prov "address" }}/{{ .NetmaskToCIDR (get $prov "netmask") }}
        {{ end }}
        {{ if hasKey $prov "gateway" }}
        provisioner.gw={{ get $prov "gateway" }}
        {{ end }}
        {{ if hasKey $prov "interface" }}
        provisioner.interface={{ get $prov "interface" }}
        {{ end }}
        {{ if hasKey $prov "vlan" }}
        provisioner.vlan={{ get $prov "vlan" }}
        {{ end }}
        {{ if hasKey $prov "mtu" }}
        provisioner.mtu={{ get $prov "mtu" }}
        {{ end }}
        {{ end }}
        {{ end }}
        {{ if .ParamExists "dns-servers" }}
        provisioner.dns={{ first (.Param "dns-servers") }}
        {{ end }}
        {{.Param "kernel-options"}}
        --
        {{.Param "kernel-console"}}
    ppc64le:
      Loader: "core.elf"
      IsoFile: "sledgehammer-7d7c0bf77daa43785e2716ecb9177dfde8dcffbf.ppc64le.tar"
      IsoUrl: "http://rackn-sledgehammer.s3-website-us-west-2.amazonaws.com/sledgehammer/7d7c0bf77daa43785e2716ecb9177dfde8dcffbf/sledgehammer-7d7c0bf77daa43785e2716ecb9177dfde8dcffbf.ppc64le.tar"
      Kernel: "7d7c0bf77daa43785e2716ecb9177dfde8dcffbf/vmlinuz0"
      Initrds:
        - "7d7c0bf77daa43785e2716ecb9177dfde8dcffbf/stage1.img"
      BootParams: >-
        rootflags=loop
        root=live:/sledgehammer.iso
        rootfstype=auto
        ro
        liveimg
        rd_NO_LUKS
        rd_NO_MD
        rd_NO_DM
        provisioner.web={{.ProvisionerURL}}
        rs.uuid={{.Machine.UUID}}
        {{ if .ParamExists "network-data" }}
        {{ $nd := .ComposeParam "network-data" }}
        {{ if hasKey $nd "prov" }}
        {{ $prov := get $nd "prov" }}
        {{ if and (hasKey $prov "address") (hasKey $prov "netmask") }}
        provisioner.ip={{ get $prov "address" }}/{{ .NetmaskToCIDR (get $prov "netmask") }}
        {{ end }}
        {{ if hasKey $prov "gateway" }}
        provisioner.gw={{ get $prov "gateway" }}
        {{ end }}
        {{ if hasKey $prov "interface" }}
        provisioner.interface={{ get $prov "interface" }}
        {{ end }}
        {{ if hasKey $prov "vlan" }}
        provisioner.vlan={{ get $prov "vlan" }}
        {{ end }}
        {{ if hasKey $prov "mtu" }}
        provisioner.mtu={{ get $prov "mtu" }}
        {{ end }}
        {{ end }}
        {{ end }}
        {{ if .ParamExists "dns-servers" }}
        provisioner.dns={{ first (.Param "dns-servers") }}
        {{ end }}
        {{.Param "kernel-options"}}
        --
        {{.Param "kernel-console"}}
OptionalParams:
  - "kernel-console"
  - "kernel-options"
Templates:
  - Name: start4.elf
    Link: '{{.Env.PathForArch "tftp" "rpi-5.4.35/start4.elf" "rpi4"}}'
    Path: '{{.Machine.MacAddr "rpi4"}}/start4.elf'
  - Name: fixup4.dat
    Link: '{{.Env.PathForArch "tftp" "rpi-5.4.35/fixup4.dat" "rpi4"}}'
    Path: '{{.Machine.MacAddr "rpi4"}}/fixup4.dat'
  - Name: bcm2711-rpi-4-b.dtb
    Link: '{{.Env.PathForArch "tftp" "rpi-5.4.35/bcm2711-rpi-4-b.dtb" "rpi4"}}'
    Path: '{{.Machine.MacAddr "rpi4"}}/bcm2711-rpi-4-b.dtb'
  - Name: kernel8.img
    Link: '{{.Env.PathForArch "tftp" "rpi-5.4.35/kernel8.img" "rpi4"}}'
    Path: '{{.Machine.MacAddr "rpi4"}}/kernel8.img'
  - Name: stage1.img
    Link: '{{.Env.PathForArch "tftp" "rpi-5.4.35/stage1.img" "rpi4"}}'
    Path: '{{.Machine.MacAddr "rpi4"}}/stage1.img'
  - Name: root.squashfs
    Link: '{{.Env.PathForArch "tftp" "rpi-5.4.35/root.squashfs" "rpi4"}}'
    Path: '{{.Machine.MacAddr "rpi4"}}/root.squashfs'
  - Name: "rpi-cmdline.txt"
    Path: '{{.Machine.MacAddr "rpi4"}}/cmdline.txt'
    Contents: |
      snd_bcm2835.enable_hdmi=1 dwc_otg.lpm_enable=0 console=serial0,115200 console=tty1 fsck.repair=yes fsck.mode=auto elevator=deadline rootwait initrd=stage1.img rw cgroup_memory=1 cgroup_enable=memory BOOTIF=discovery provisioner.web={{.ProvisionerURL}} rs.uuid={{.Machine.UUID}}
  - Name: rpi4b-config.txt
    Path: '{{.Machine.MacAddr "rpi4"}}/config.txt'
    Contents: |
      # uncomment if you get no picture on HDMI for a default "safe" mode
      hdmi_safe=1

      # uncomment this if your display has a black border of unused pixels visible
      # and your display can output without overscan
      disable_overscan=1

      # uncomment the following to adjust overscan. Use positive numbers if console
      # goes off screen, and negative if there is too much border
      #overscan_left=16
      #overscan_right=16
      #overscan_top=16
      #overscan_bottom=16

      # uncomment to force a console size. By default it will be display's size minus
      # overscan.
      #framebuffer_width=1280
      #framebuffer_height=720

      # uncomment if hdmi display is not detected and composite is being output
      #hdmi_force_hotplug=1

      # uncomment to force a specific HDMI mode (this will force VGA)
      #hdmi_group=1
      #hdmi_mode=1

      # uncomment to force a HDMI mode rather than DVI. This can make audio work in
      # DMT (computer monitor) modes
      #hdmi_drive=2

      # uncomment to increase signal to HDMI, if you have interference, blanking, or
      # no display
      #config_hdmi_boost=4

      # uncomment for composite PAL
      #sdtv_mode=2

      # Uncomment some or all of these to enable the optional hardware interfaces
      #dtparam=i2c_arm=on
      #dtparam=spi=on
      #dtparam=i2s=on

      # Uncomment this to enable infrared communication.
      #dtoverlay=gpio-ir,gpio_pin=17
      #dtoverlay=gpio-ir-tx,gpio_pin=18

      # Enable audio (loads snd_bcm2835)
      dtparam=audio=on

      [pi4]
      dtoverlay=vc4-fkms-v3d
      max_framebuffers=2
      arm_64bit=1
      #device_tree_address=0x03000000

      [all]
      #dtoverlay=vc4-fkms-v3d
      kernel kernel8.img
      initramfs stage1.img
  - Name: "kexec"
    ID: "kexec.tmpl"
    Path: "{{.Machine.Path}}/kexec"
  - ID: "default-pxelinux.tmpl"
    Name: "pxelinux"
    Path: "pxelinux.cfg/{{.Machine.HexAddress}}"
  - ID: "default-ipxe.tmpl"
    Name: "ipxe"
    Path: "{{.Machine.Address}}.ipxe"
  - ID: "default-pxelinux.tmpl"
    Name: "pxelinux-mac"
    Path: 'pxelinux.cfg/{{.Machine.MacAddr "pxelinux"}}'
  - ID: "default-ipxe.tmpl"
    Name: "ipxe-mac"
    Path: '{{.Machine.MacAddr "ipxe"}}.ipxe'
  - ID: "default-grub.tmpl"
    Name: "grub"
    Path: "grub/{{.Machine.Address}}.cfg"
  - ID: "default-grub.tmpl"
    Name: "grub-mac"
    Path: 'grub/{{.Machine.MacAddr "grub"}}.cfg'
  - Name: grub-http-boot
    Path: '{{.Env.PathFor "tftp" ""}}/97064f6a618603ffd6f37670dafa81b41a00b5cc/grub.cfg'
    ID: default-grub.tmpl
  - ID: drp-agent-cfg.yaml.tmpl
    Name: agent-cfg
    Path: "{{.Machine.Path}}/agent-cfg.yml"
  - ID: "control.sh.tmpl"
    Name: "control.sh"
    Path: "{{.Machine.Path}}/control.sh"