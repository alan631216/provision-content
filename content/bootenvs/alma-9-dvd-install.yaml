---
Name: alma-9-dvd-install
Description: AlmaLinux 9 DVD installer
Documentation: |
  AlmaLinux 9 DVD installer that points to the latest Alma 9 release.

  ISOs can be downloaded from:

    * <https://mirrors.almalinux.org/isos.html>
Meta:
  color: blue
  feature-flags: change-stage-v2
  icon: linux
  title: Digital Rebar Community Content
  type: os
  group-by: Alma
OptionalParams:
  - operating-system-disk
  - provisioner-default-password-hash
  - kernel-console
  - kernel-options
  - proxy-servers
  - select-kickseed
OnlyUnknown: false
Loaders:
  amd64-uefi: EFI/BOOT/BOOTX64.EFI
  arm64-uefi: EFI/BOOT/BOOTAA64.EFI
OS:
  Name: alma-9-dvd
  Family: redhat
  Codename: alma
  Version: 9.3
  SupportedArchitectures:
    x86_64:
      IsoFile: AlmaLinux-9.3-x86_64-dvd.iso
      Sha256: 4a8c4ed4b79edd0977d7f88be7c07e12c4b748671a7786eb636c6700e58068d5
      IsoUrl: https://repo.almalinux.org/almalinux/9.3/isos/x86_64/AlmaLinux-9.3-x86_64-dvd.iso
      Kernel: images/pxeboot/vmlinuz
      Initrds:
        - images/pxeboot/initrd.img
      BootParams: >-
        ksdevice=bootif
        inst.ks={{.Machine.Url}}/compute.ks
        inst.repo={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
    aarch64:
      IsoFile: AlmaLinux-9.3-aarch64-dvd.iso
      Sha256: 9447a80794d83678e6746b590f2ac4596396b3d2f14f91817ffdb93bd05fbeb7
      IsoUrl: https://repo.almalinux.org/almalinux/9.3/isos/aarch64/AlmaLinux-9.3-aarch64-dvd.iso
      Kernel: images/pxeboot/vmlinuz
      Initrds:
        - images/pxeboot/initrd.img
      BootParams: >-
        ksdevice=bootif
        inst.ks={{.Machine.Url}}/compute.ks
        inst.repo={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
    ppc64le:
      IsoFile: AlmaLinux-9.3-ppc64le-dvd.iso
      Sha256: 0283610dc23fedfdf4fa7ba4d4597267bfcc7893503e155b28b37fe72ebc9551
      IsoUrl: https://repo.almalinux.org/almalinux/9.3/isos/ppc64le/AlmaLinux-9.3-ppc64le-dvd.iso
      Loader: boot/grub/powerpc-ieee1275/core.elf
      Kernel: images/pxeboot/vmlinuz
      Initrds:
        - images/pxeboot/initrd.img
      BootParams: >-
        ksdevice=bootif
        inst.ks={{.Machine.Url}}/compute.ks
        inst.repo={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
Templates:
  - Name: kexec
    Path: "{{.Machine.Path}}/kexec"
    ID: kexec.tmpl
  - Name: pxelinux
    Path: "pxelinux.cfg/{{.Machine.HexAddress}}"
    ID: default-pxelinux.tmpl
  - Name: ipxe
    Path: "{{.Machine.Address}}.ipxe"
    ID: default-ipxe.tmpl
  - Name: pxelinux-mac
    Path: 'pxelinux.cfg/{{.Machine.MacAddr "pxelinux"}}'
    ID: default-pxelinux.tmpl
  - Name: ipxe-mac
    Path: '{{.Machine.MacAddr "ipxe"}}.ipxe'
    ID: default-ipxe.tmpl
  - Name: grub
    Path: "grub/{{.Machine.Address}}.cfg"
    ID: default-grub.tmpl
  - Name: grub-mac
    Path: 'grub/{{.Machine.MacAddr "grub"}}.cfg'
    ID: default-grub.tmpl
  - Name: grub-http-boot
    Path: '{{.Env.PathFor "tftp" ""}}/EFI/BOOT/grub.cfg'
    ID: default-grub.tmpl
  - Name: compute.ks
    Path: "{{.Machine.Path}}/compute.ks"
    ID: select-kickseed.tmpl
