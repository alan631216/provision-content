#!/usr/bin/env bash
# Attempts to automatically select Linux BootEnv version, or set to Operator defined version

###
#  This tool selects the Linux Install BootEnv version to install on the system.
#  The operator may specify a linux via the use of the "linux/install-bootenv"
#  Param (which is an enumerated list), or via use of the override Param if the
#  version is not listed in the enum list ("linux/install-bootenv-override").
#
#  The linux/install-bootenv will use a map to get the bootenv of choice.
#  The map can be overriden by using linux/install-bootenv-map.
###
{{ template "setup.tmpl" . }}
{{ if .ParamExists "linux/install-bootenv-override" -}}
BOOTENV="{{ .Param "linux/install-bootenv-override"}}"
{{ else -}}
BOOTENV_INDEX="{{ .Param "linux/install-bootenv" }}"
BOOTENV=$(echo '{{ .ParamAsJSON "linux/install-bootenv-map" }}' | jq -r ".[\"$BOOTENV_INDEX\"]")
{{ end -}}

[[ -z "$BOOTENV" ]] && xiterr 1 "Bootenv is unset, this shouldn't have happend."
[[ "$BOOTENV" == "null" ]] && xiterr 1 "Bootenv is not in map, this shouldn't have happend."

if [[ "$BOOTENV" =~ -install$ ]]
then
  echo "Specified bootenv ends with '-install' - this is good!"
else
  echo "Specified bootenv does not end with '-install' - this is bad!"
  exit 1
fi

if ! drpcli bootenvs exists "$BOOTENV"; then
  xiterr 1 "BootEnv $BOOTENV does not exist"
fi
echo ">>> Requesting install of BootEnv: '$BOOTENV'"
env="$(drpcli bootenvs show "$BOOTENV")"
if [[ $(jq '.Available' <<< $env) != true ]]; then
  xiterr 1 "Bootenv $BOOTENV is not available"
fi

drpcli machines tasks add {{.Machine.UUID}} at 0 bootenv:${BOOTENV} 1>/dev/null
