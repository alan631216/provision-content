
The Vmware Library provides additional content that enables capabilities for
interacting with VMware ESXi, vSphere, and VCF (VMware Cloud Foundation)
environments.

This content provides *Software Defined Data Center* (SDDC) build and operational
management workflows.

Some examples of operations that can be achieved with this workflow:

  * deploy vCenter on one or more ESXi nodes
  * create a *Datacenter* construct in vCenter
  * create a *Cluster* construct in vCenter
  * enroll specified ESXi machines in to the *Cluster*
  * configure VSAN datastore capabilities
  * claim disks in to the VSAN datastore
  * deploy arbitrary OVA appliance devices
  * deploy VMware Cloud Foundations VCF clusters via Cloud Builder deployed OVA
  * create and manage other vSphere/vCenter related resources, like:

    * Virtual Switches and Distributed Virtual Switches (DVS/vDS)
    * Portgroup create/destroy/management
    * Rename objects in vCenter inventory
    * Guest Virtual Machine management
    * Pools, Roles, Sessions, Snapshot management
    * Any `govc` (GoVMOMI library) based commands, documented at:
      <https://github.com/vmware/govmomi/blob/master/govc/USAGE.md>

This content pack utilizes Digital Rebar Provision (DRP) Context containers
to perform most of the heavy lifting work.  For general use, start by creating
a Context Machine (Machine that is backed by a Context / BaseContext), to
utilize the tooling to execute API actions against the vSpher/VCF target(s).

It is recommended to create a Context container that correlates to a given
resource that is being managed.  This allows for ongoing lifecycle management
of the managed service.

It is possible to move many of the functions in to WorkOrders and Blueprints,
however, this work has not been done in the `vmware-lib` content pack yet.

There are several tools which are available and used, based on the tools
capabilities, and the required job at hand:

  * `govc` - a Golang compiled binary which implements the *GoVMOMI* library
  * VMware's Python SDK
  * VMware's Ansible *Galaxy* modules, which utilize PyVMOMI and Python SDK libraries
  * OVFTool
  * *etc.*

All of the content provided in `vmware-lib` builds upon, and requires the `vmware`
plugin to be installed, and generally relies on ESXi nodes to be built by a workflow
like the `esxi-install` workflow.  It is feasible that setting the appropriate Param
values will allow this content to work on non-DRP built ESXi nodes; however this is
not tested nor advised.

The `govc` context container is well suited for managing ongoing operations (in
addition to initial creation operations).  The only primary downside to use of
the `govc` binary and API interactions is related to the poor and inconsistent
exit code handling of the various different sub-commands.


## Prerequisites

The following prerequisites must be met to support the `vmware-lib` capabilities.
Note that this is in addition to the necessary VMware vSphere/VCF/vCenter infrastructure
that is being automated and orchestrated.

  * DRP Endpoint version v4.8.0 or newer
  * DRP Community Content v4.8.0 or newer
  * VMware Plugin v4.8.0 or newer
  * VMware Library (`vmware-lib`) v4.8.0 or newer
  * the `govc` context installed and bootstrapped (1)
  * the `vmware-tools` context installed and bootstrapped (1)

Note 1:  Both context containers should be installed and tested (eg run the
`hello-world` workflow in a test Machine backed by the Context).  Please
see [Install Context Containers with 'drpcli'
](../../../resources/kb/kb-00080) for instructions on how to
ensure the Context containers are installed and working.


## GoVC General Information

GoVC is a Golang binary that implements the VMOMI library of capabilities.  The
primary benefit is it's a single statically compiled binary (stand alone) that has
no external dependencies.  It implements API interaction with vSphere and services
(eg vSphere ESXi, vSphere vCenter, VMWare Cloud Foundations/VCF).

The GoVC binary (`govc`) is compiled from the GoVMOMI project, which can be
found at:

  * <https://github.com/vmware/govmomi>

The GoVC tool is capable of an extremely broad and complete set of control plane
interactions with vSphere (ESXi and vCenter) services.  Please review the
`examples` directory in the above referenced Repo for more details.

For usage examples of the `govc` binary in use inside the `govc` context
container, please see:

  * <https://github.com/vmware/govmomi/blob/master/govc/USAGE.md>


### Context Usage

The `vmware-lib` tooling utilizes two primary Context containers that
have the embedded VMware tooling in them.  They are:

  * `govc`: lightweight container with just `govc` binary (based on GoVMOMI library)
  * `vmware-tools`: big fat bloated container with everything and the kitchen sink

Both context containers implement a RackN Context with the Agent (runner, drpcli
binary) and the associated binaries and libraries in them.  By use of setting Param values,
`govc` commands can be executed against vSphere resources.

Please see [Install Context Containers with 'drpcli'
](../../../resources/kb/kb-00080) for setup instructions.  Ensure
that the `govc` and `vmware-tools` Contexts are fully installed based on these
instructions.


### GoVC and VCSA Deployment

VCSA (vCenter Server Appliance) can be deployed via the GoVC tool.  The operator must
perform the following preparotry tasks to enable the Context environment to operate
the `govc` binary in the RackN Context Container.  This setup must be performed on
the DRP Endpoint.  In the future, the _bootstrap_ workflows will be available to help
set up these environments.

**Setup Instructions**

  - The VCSA OVA must be staged on an HTTP server for the tooling to download

    * Obtain the VMware provide VCSA ISO image and extract the OVA from the ISO
    * example download location - <https://my.vmware.com/web/vmware/details?productId=742&rPId=39682&downloadGroup=VC67U3B>
    * can be extracted with `bsdtar` like: `bsdtar -xvf VMware-VCSA-all-6.7.0-15132721.iso vcsa/*.ova`
    * upload with drpcli like: `export N=$(ls -1 vcsa/*.ova); drpcli files upload $N as images/vcsa/$N`
    * reference this location on the DRP endpoint as: `{{.ProvisionerURL}}/files/images/vcsa/{...name...}`

  - Prepare the Template JSON file that GoVC will use to deploy the OVA (***see below***)
  - Set the Param values on your Runner fake machine (either directly. or as a Profile)
  - Run the Workflow `govc-vcenter-create`

Scripts referenced in this document should be available from:

  * <https://gitlab.com/rackn/provision-content/tree/v4/vmware-lib>

### Use of `govc/debug` Param

The `govc/debug` Param can be set to add more logging output to Jobs Logs, without
setting the `rs-debug-enable`, which can be a lot of debugging.  Not all tasks
support enhanced Job Log output via `govc/debug`, but many do.

!!! warning
    In many cases, username and password values WILL BE revealed in Job Logs
    when `govc/debug` is set.  This allows password credential debugging
    in those cases.  Care must be taken when using this feature.

### vCenter Complete Note

If install vCenter 7.x - the `govc` connect URL method seems to have changed.
AS of 2020/07/01 - the Stage `govc-wait-for-vcenter` will not complete successfully.
You will have to monitor the VAMI web interface (on port 5480 by default), to determine
when it has successfully finished.

The workflow will error out after 60 minutes in this case.  Either force remove the
Workflow from the Context Machine, or ignore the status stage error.


### Prepare the VCSA JSON Deployment Param

The Param `ova/param-json` provides the JSON data configuration used during the OVA
deployment process, to configure the OVA.  See the Profile `EXAMPLE-govc-vcsa-vc01.yaml`
examples in the `vmware-lib` content pack for an example template.

Once you have prepared the Template JSON file and uploaded it, you must set the Param
to point to it.  This param will be set on the Context Runner Machine that the Workflow
is run on.

In addition to the Template JSON Param, you must provide a vSphere resource (eg ESXi) node
to execute the deployment to.  Set these Params as defined in the below section.


### Define the Deployment Target

You must define the vSphere deployment target (eg ESXi node) to deploy the VCSA OVA to.
This is done by specifying the URL directly as a single Param, or the individual Param
values for the Username, Password, Node, and optionally Port.  See the Param documentation
for these values.

These values can all be combined in to a single Profile along with the Template JSON
Param defined above for easier add/remove on the Machine object.

Example Profile for vCenter deployment:

  ```yaml
  ---
  Name: "vcsa-govc-esxi-ewr1"
  Description: "EXAMPLE PROFILE - CHANGE VALUES !!!!"
  Documentation: |
    Change these values to match the JSON template details, the
    uploaded OVA, and related network information for your vCenter
    deployment.

    govc/* params are for the target Node (vSphere ESXi) to deploy the
    vCenter VCSA OVA on.  The JSON Template defines the vCenter
    installation details.

  Meta:
    color: "blue"
    icon: "hdd"
    title: "Digital Rebar"
  Profiles: []
  Params:
    govc/datastore: "datastore1"
    govc/datastore-skip-create: false
    govc/insecure: true
    govc/node: "10.75.75.250"
    govc/ova-location: "{{.ProvisionerURL}}/files/images/vcsa/VMware-vCenter-Server-Appliance-7.0.0.10300-16189094_OVF10.ova"
    govc/username: "root"
    govc/password: "VMware123"
    ova/param-json: |
      ...JSON data structure here...
      ...see profiles/EXAMPLES-govc-vcsa-vc01.yaml for an example...
  ```

Save the above to file, and use drpcli to add to your Endpoint (eg `drpcli profiles create vcenter.yaml`,
then add the Profile to the Context Machine that will deploy the vCenter VCSA OVA.


## Example GOVC Usage

A (begining of) a collection of useful resources for understanding how to
use `govc` to manage vSphere resources.

  * [Collabnix Labs: Managing vSphere with GOVC](https://github.com/collabnix/govc)


## VCF Cloud Builder cluster bootstrap

This content supports VMware Cloud Foundations (VCF) bootstrap bringup via use of the
Cloud Builder virtual machine appliance.  The process is generally tackled with the
following content components:

  * operator must construct a valid `bringup.json` spec file for the final VCF cluster build
  * construct the appropriate `govc/*` Params, and JSON Template config for the Cloud Builder deployment
  * deploy the Cloud Builder OVA with the `esxi-sddc-ovftool-deploy` workflow to an ESXi host
  * start the bootstrap process of the VCF cluster with `esxi-sddc-manage` workflow, see the
    Task of the same name for supported Operations (eg about, validate, create, create_retry, etc)

The following example content esists to help guide with this process:

  * Profile `EXAMEPLE-vcf-sddc-cloud-builder.yaml` - contains Cloud Builder OVA deployment and operation config
  * Templates `EXAMPLE-gamble-vcf-bringup.json.tmpl` - VCF Cluster bringup JSON spec

