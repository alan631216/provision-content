---
Description: "Attempts to update the 'govc' binary in the context container at runtime"
Name: "govc-update-version"
Documentation: |
  This task will attempt to update the `govc` binary inside the context
  container if it can.  This task requires outbound internet access to
  `github.com` releases by default.  The URL can be overridden if a binary
  is staged somewhere else.

  To use this Task, the Param `govc/update-binary` must be set to `True`;
  by default it is `False`.

  The default download will be performed against the following HTTP URL
  location:

  ```shell
  # $VER will be replaced by 'govc/update-version` Param value
  https://github.com/vmware/govmomi/releases/download/$VER/govc_Linux_x86_64.tar.gz
  ```

  If the `govc/update-url` is set, it will override the above default
  location.  You can specify Golang template constructs in the URL if
  desired, like:

  ```
  "govc/update-url": 'https:/host.example.com/{{ .Param "govc/update-version" }}/govc_Linux_x86.64.tar.gz'
  ```

  Assuming `govc/update-version` is set to `v0.30.4`, this Will expand to
  something like:

  - `https:/host.example.com/v0.30.4/govc_Linux_x86.64.tar.gz`

ExtraClaims:
  - scope: "machines"
    action: "*"
    specific: "*"
Meta:
  icon: "download"
  color: "purple"
  title: "Digital Rebar Community Content"
  feature-flags: "sane-exit-codes"
OptionalParams:
  - "govc/update-binary"
  - "govc/update-version"
  - "govc/update-url"
Templates:
  - Name: "govc-update-binary.sh"
    Contents: |
      #!/usr/bin/env bash
      # Update the 'govc' binary in a running context container.
      # RackN Copyright 2023

      {{ if ( .Param "govc/update-binary" ) -}}
      echo "INFO: Starting 'govc' binary update process"
      {{ else -}}
      echo "INFO: Not updating 'govc' binary ('govc/update-binary' is 'false')"
      exit 0
      {{ end -}}

      ### setup.tmpl
      {{ template "setup.tmpl" .}}

      # if a specific version is desired and the default download URL location
      # is used, then you only need to update 'govc/update-version'
      URL="{{ .ParamExpand "govc/update-url" }}"
      DL="$(mktemp -u).tgz"
      GOVC=$(which govc 2> /dev/null || true)

      if [[ -z "$GOVC" ]]
      then
        xiterr 1 "No 'govc' binary found in PATH on system to update"
      fi

      echo ">>> Using final rendered download URL location:"
      echo "    '$URL'"
      ORIG_VER=$("$GOVC" version 2> /dev/null | sed 's/^govc //g')
      cd /tmp/
      curl -fsSLk -o "$DL" "$URL"

      # tar will extract compressed and uncompressed TAR archives
      # without specifying zip format, regardless of filename extension
      set +e
      tar -xf "$DL" govc 2> /dev/null
      ERR=$?
      set -e

      if (( $ERR ))
      then
        echo ">>> Unable to extract 'govc' binary from download file."
        echo ">>> Will try to use it as a binary directly"
        mv "$DL" govc
      fi

      chmod 755 govc
      VER=$(./govc version 2> /dev/null | sed 's/^govc //g')

      if [[ -z "$VER" ]]
      then
        echo '!!! Unable to determine format of download file and use it'
        echo '!!! Tried TGZ, TAR, and raw binary execution.'
        xiterr 1 "Unsupported download file type (expect TGZ, TAR, or just binary file."
      else
        cp govc "$GOVC"
        echo ">>> Updated '$GOVC' to version '$VER' from '$ORIG_VER'"
      fi

      echo ">>> This 'govc' version supports the following sub-options"
      # because they make bad commands that don't output a usage statement
      # without producing an error exit value ... so dumb ... 
      "$GOVC" --help || true

      {{ if ( .Param "rs-debug-enable" ) -}}
      echo "WARN: Not cleaning up /tmp dir artifacts, 'rs-debug-enable' is 'true'"
      {{ else -}}
      rm -f "$DL" /tmp/govc
      {{ end -}}
