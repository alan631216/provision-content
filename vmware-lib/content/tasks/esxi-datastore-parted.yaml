---
Name: "esxi-datastore-parted"
Description: "Create an ESXi datastore via partedUtil and vmkfstools"
Documentation: |
  This task runs inside ESXi and uses the `partedUtil` and `vmkfstools`
  to create an ESXi Datastore.

  It parses the `esxi/datastore-mappings` Param, allowing for unified
  datastore creation via this Task, or the `govc-datastore-manage`
  Task which uses external API calls.  In some cases, those external
  API calls do not work.

  The `esxi/datastore-mappings` structure must have a `"tool": "parted"`
  directive to specify that the Datastore should be created via this tooling.
  Any datastore definition without this, or with the `"tool": "govc"` will
  be managed by the `govc-datastore-manage` Task.

  See the `esxi/datastore-mappings` Param for detailed usage documentation.
  The `esxi/datastore-memberships` Param is ignored in this Task, if the
  Machine has the mappings Param, and it's set to `parted` as the tool,
  the datastore will be attempted to be created.

Meta:
  icon: "terminal"
  color: "blue"
  title: "Digital Rebar Community Content"
  feature-flags: "sane-exit-codes"
RequiredParams:
  - "esxi/datastore-mappings"
OptionalParams:
  - "esxi/datastore-skip-manage"
Templates:
  - Name: "esxi-datastore-parted.sh"
    Contents: |
      #!/usr/bin/env sh
      # Create ESXi datastore(s) via partedUtil and vmkfstools
      # Runs inside ESXi - must by busybox shell compliant
      # RackN Copyright 2022

      set -e
      {{ if .Param "rs-debug-enable" }}set -x{{ end }}

      function xiterr() { XIT=$1; shift; printf "FATAL: $*\n"; exit $XIT; }

      {{ if .Param "esxi/datastore-skip-manage" -}}
      echo "Skipping datastore creation; 'esxi/datastore-skip-manage' is 'true'."
      exit 0
      {{ end -}}

      ###
      #  Create a VMFS datastore with "$1" name, "$2" type
      #  (eg 'vmfs5' or 'vmfs6'), on "$3" disk device
      #
      #  Assumes datastore is whole disk, and GPT type
      ###
      function vmfs_create() {
        _name="$1"
        _ver="$2"
        _disk="$3"

        echo ""
        echo "==== RUN datastore create for '$_name' ===="
        echo ""

        # wipes disk and sets empty GPT label
        echo "+++ Creating GPT partition table label"
        partedUtil mklabel "$_disk" gpt

        _guid=$(partedUtil showGuids | grep "^ vmfs " | awk ' { print $NF } ')
        [[ -z "$_guid" ]] && xiterr 1 "Unable to get 'vmfs' filesystem GUID" || true

        # sigh - because busybox shell is soooooo limited
        _start=$(partedUtil getUsableSectors "$DS_DISK" | awk ' { print $1 } ')
        _end=$(partedUtil getUsableSectors "$DS_DISK" | awk ' { print $2 } ')
        [[ -z "$_start" ]] && xiterr 1 "could not get start sector from '$DS_DISK'" || true
        [[ -z "$_end" ]] && xiterr 1 "could not get end sector from '$DS_DISK'" || true

        echo ""
        echo "           name: '$_name'"
        echo "     filesystem: '$_ver'"
        echo "           disk: '$_disk'"
        echo "   start sector: '$_start'"
        echo "     end sector: '$_end'"
        echo "filesystem guid: '$_guid'"
        echo ""

        echo ""
        echo "+++ Running 'partedUtil setptbl'"
        partedUtil setptbl "$_disk" gpt "1 $_start $_end $_guid 0"

        echo ""
        echo "+++ Running 'vmkfstools' create filesystem"
        vmkfstools -C $_ver -b 1m -S "$_name" "${_disk}:1"

        echo ""
        echo ">>> Datastore volume information:"
        df -h "/vmfs/volumes/$_name"

        echo ""
        echo "==== DONE ===="
        echo ""
      }

      # because our agent stdout needs an extra line ...
      echo ""

      {{ if .Param "esxi/datastore-mappings" -}}
        echo ">>> Datastore mappings found, starting processing..."

        {{ range $dsref, $dsval := ( .ComposeParam "esxi/datastore-mappings" ) -}}
        REF='{{ $dsref }}'
          {{ if eq ( get $dsval "tool" ) "parted" }}
          DS_TOOL='{{ get $dsval "tool" }}'
          DS_NAME='{{ get $dsval "name" }}'
          DS_TYPE='{{ get $dsval "type" }}'
          DS_DISK='{{ get $dsval "disk" }}'
          case "{{ get $dsval "version" }}" in
            5|vmfs5) DS_VER="vmfs5" ;;
            6|vmfs6) DS_VER="vmfs6" ;;
            *) DS_VER="vmfs6" ;;
          esac

          case $DS_DISK in
            /dev*|/vmfs*) true ;;
            *) DS_DISK="/vmfs/devices/disks/$DS_DISK" ;;
          esac

          if ls -1 "$DS_DISK"
          then
            echo ">>> Datastore create set to disk device:"
            echo "    '$DS_DISK'"
          else
            xiterr 1 "Disk '$DS_DISK' device doesn't exist"
          fi

          # if 2 or more, there are existing partitions on the disk
          if [[ "$(partedUtil get $DS_DISK | wc -l)" -gt 1 ]]
          then
            xiterr 1 'Existing partitions on disk, disk must be empty'
          fi

          case $DS_TYPE in
            vmfs)
              vmfs_create "$DS_NAME" "$DS_VER" "$DS_DISK"
            ;;
            *)
              xiterr 1 "Datastore type '$DS_TYPE' not supported."
            ;;
          esac
          {{ else -}}
          echo "--- '$REF' not requested to be created by 'parted' tool - skipping"
          {{ end -}}
        {{ end -}}
      {{ else -}}
        echo ">>> Param 'esxi/datasotre-mappings' has no configuration.  Skipping."
      {{ end -}}

      exit 0
